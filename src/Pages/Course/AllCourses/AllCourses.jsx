import React, { useState } from 'react'
import { useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';



  

export default function AllCourses() {

  const [allcourses , setAllCourses] = useState([]);
  const navigate = useNavigate()




  useEffect(() => {
    // const userFromSessionStorage = sessionStorage.getItem('userId');
    const url = "http://localhost:8000/api/all-courses-published"
    axios.get(url).then((response) => {
      console.log(response.data)
      setAllCourses(response.data)
    })
      .catch((error) => {
        console.log(error.data)
      });
    },[]);


    const handleClick = (item) => {
      // console.log(item)
      sessionStorage
        .setItem("selectedCourseId",item)
        navigate('/courseEnroll');
    };

  return (
    <div className='AllCoursesTop pt-5'>
      <h2>AllCourses</h2>

      {allcourses && allcourses.map((course)=>(
        <div className='CourseInfoCardAll border border-primary mb-2 d-flex justify-content-between'>
          <div>
          <div className="courseName">
            {course.name}
          </div>
          <div className='coursePrice'>
            {course.price}
          </div>
          </div>
          <div className="nextPageIcon mx-5">
          {/* <Link to={{ pathname: '/courseEnroll', state: course.id }}>
          <i class="fa-solid fa-forward"></i>
          </Link> */}
          <i class="fa-solid fa-forward" onClick={() => handleClick(course._id)}></i>
          </div>
        </div>
      ))
      }
    </div>
  )
}
