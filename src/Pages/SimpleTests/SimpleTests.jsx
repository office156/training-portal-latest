import React from 'react'
import { useState } from 'react';
import LinearProgressWithLabel from '../../Components/LinearProgressBar/ProgressBar';
import "./tests.css"

export default function SimpleTests() {

    const [checkedCount, setCheckedCount] = useState(0);
    const [progress , setprogress] = useState(0);
    const checkboxes = [
      { id: 1, label: 'Option 1' },
      { id: 2, label: 'Option 2' },
      { id: 3, label: 'Option 3' },
    ];
  
    const handleCheckboxChange = (e) => {
      const checkboxId = parseInt(e.target.value);
      if (e.target.checked) {
        setCheckedCount(checkedCount + 1);
        let count = checkedCount + 1
        let currprogress = (count / 3) * 100
        console.log(currprogress)
        setprogress(currprogress)
        // setprogress((count / 3) * 100)
      } else {
        setCheckedCount(checkedCount - 1);
        let count = checkedCount - 1
        let currprogress = (count / 3) * 100
        console.log(currprogress)
        setprogress(currprogress)
      }
    };

  return (
    <div>
        <h1>SimpleTests</h1>

        <div>
      <h2>Checkbox Counter</h2>
      <p>Checked Count: {checkedCount}</p>

<div className='w-100 justify-content-center d-flex align-items-center'>
<span className="me-2">Progress bar : </span>
      <div className="progressbar">
        
      <LinearProgressWithLabel value={progress} />
      </div>
    </div>
      {checkboxes.map((checkbox) => (
                <label key={checkbox.id}>
                <input
                  type="checkbox"
                  value={checkbox.id}
                  onChange={handleCheckboxChange}
                />{' '}
                {checkbox.label}
              </label>
      ))}
      
    </div>

    
    </div>
  )
}
