import React, { useState , useRef } from 'react';

function FileUpload() {
  const [file, setFile] = useState(null);
  const inputRef = useRef(null);

  const handleFileInputChange = (event) => {
    const selectedFile = event.target.files[0];
    setFile(selectedFile);
  };

  const handleButtonClick = () => {
    inputRef.current.click();
  };


  return (
    <div>
      <form >
        <input type="file" ref={inputRef} hidden onChange={handleFileInputChange} />
        <button type="button" className="btn" onClick={handleButtonClick}>
          {/* <FontAwesomeIcon icon="fa-regular fa-cloud-arrow-up" /> */}
          <i class="fa-solid fa-cloud-arrow-up"></i>
          </button>
      </form>
      {file && (
        <div>
          <p>File name: {file.name}</p>
          <p>File type: {file.type}</p>
          <p>File size: {file.size} bytes</p>
        </div>
      )}
    </div>
  );
}

export default FileUpload;
