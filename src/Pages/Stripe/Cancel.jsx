import React from 'react'
import { CloudSyncOutlined } from "@mui/icons-material"

export default function Cancel() {
  return (
    <div>
      <CloudSyncOutlined className=""></CloudSyncOutlined>
      <p className="lead">Payment failed. Try again.</p>
    </div>
  )
}
