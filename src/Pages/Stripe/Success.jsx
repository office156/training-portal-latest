import React, { useEffect } from 'react'
import { CloudSyncOutlined } from '@mui/icons-material'
import axios from 'axios'
import { useNavigate } from 'react-router-dom';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
// import useRouter 

export default function Success() {
  // const userFromSessionStorage = sessionStorage.getItem('userId');
  const token = sessionStorage.getItem('Token');
  const courseFromSessionStorage = sessionStorage.getItem('selectedCourseId');

  const navigate = useNavigate()
  useEffect(()=>{
    successRequest();
  },[]);

  const successRequest = async () =>{
    try {
      const url = "http://localhost:8000/api/course/enroll/success/" + courseFromSessionStorage
      // const token ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NDNmZGI1NjFlMzA0Mjk1ODliYzFlNDAiLCJpYXQiOjE2ODE5MDkzNTgsImV4cCI6MTY4MjUxNDE1OH0.dxZffsCVIK2jicqO0osZGSmb8DCqSWZlXfxKisr_spM"
      const {data} = await axios.post(url,null,{
          headers: {
            'Authorization': `Bearer ${token}`
          }
        })


        console.log(data)
        // navigate("/user-profile")
        // window.location.href = data


      // const stripe = await loadStripe(process.env.NEXT_PUBLIC_STRIPE_KEY);
      // // stripe.red
      // stripe.redirectToCkeckout({ sessionId : data});
  } catch (error) {
      console.log(error)
      navigate("/checkout-cancel")
  }
  }

  return (
    <Box sx={{ display: 'flex' }}>
    <CircularProgress />
    <button onClick={()=>{
      navigate('/user-profile')
    }}>Back</button>
  </Box>
  )
}
