import React, { useState } from 'react'
import Sidebar from '../../Components/Sidebar/Sidebar'
import { useEffect } from 'react';
import axios from 'axios';
import CourseDetails from './CourseDetails';
import UserProfile from './UserProfile';

export default function Statistics() {

    const [topusers , settopusers] = useState([])
    const [topcourses , settopcourses] = useState([])
    const [userextra , setuserextra] = useState()
    const [courseextra , setcourseextra] = useState()
    const [adminname , setadminname] = useState("")
    const [adminmail , setadminmail] = useState("")
  


    useEffect(() => {
        // const userFromSessionStorage = sessionStorage.getItem('userId');
        // console.log(courseFromSessionStorage)
        const url = "http://localhost:8000/api/top-users" 
        axios.get(url).then((response) => {
            settopusers(response.data)
        })
          .catch((error) => {
            console.log(error.data)
          });
        const url2 = "http://localhost:8000/api/top-courses" 
        axios.get(url2).then((response) => {
          console.log(response.data)
            settopcourses(response.data.topCourses)
        })
          .catch((error) => {
            console.log(error.data)
          });
          setadminmail(sessionStorage.getItem("email"))
          setadminname(sessionStorage.getItem("name"))
        },[]);


        const fetchuserextra = (id) =>{
          // /admin/user/details/id/
          console.log("In fetch user")
          const url = "http://localhost:8000/api/admin/user/details/id/" + id
          axios.get(url).then((response) => {
            console.log(response.data)
              setuserextra(response.data)
          })
            .catch((error) => {
              console.log(error.data)
            });
        }

        const fetchcourseextra = (id) =>{
          console.log("In fetch course")
          // /admin/user/details/id/
          const url = "http://localhost:8000/api/admin/coursedetails/" + id
          axios.get(url).then((response) => {
            console.log(response.data)
              setcourseextra(response.data)
          })
            .catch((error) => {
              console.log(error.data)
            });
        }


  return (
    <div>
            <div className="adminPanelTop">
      <Sidebar className = "adminPanelSidebartag" name={adminname} mail={adminmail}></Sidebar>


<div className="contentAdminPanel">
  <div className="container">
    <h1 className='text-decoration-underline'>Statistics</h1>
  </div>
  <div className="statisticsTables mt-5 container">
    <div className="table1">
        <h3>Top users</h3>
    <table class="table mt-3">
  <thead>
    <tr>
      {/* <th scope="col">#</th> */}
      <th scope="col">Name</th>
      <th scope="col">Enrolled courses</th>
      <th scope="col">Extra info</th>
      {/* <th scope="col">Handle</th> */}
    </tr>
  </thead>
  <tbody>
  {topusers && topusers.map((user)=>(
    <tr>
    {/* <th scope="row">1</th> */}
    <td>{user.name}</td>
    <td>{user.enrolledCoursesCount}</td>
    <td>
      <button data-bs-toggle="modal" data-bs-target="#staticBackdrop2" onClick={() => {
      fetchuserextra(user._id)}
      }>
      <i class="fa-solid fa-chevron-down" ></i>
      </button>
    </td>
  </tr>
      ))
      }

  </tbody>
</table>
    </div>
  </div>

  <div className="statisticsTables mt-5 container">
    <div className="table1">
        <h3>Top courses</h3>
    <table class="table mt-3">
  <thead>
    <tr>
      {/* <th scope="col">#</th> */}
      <th scope="col">Name</th>
      <th scope="col">Enrolled users</th>
      <th scope="col">Extra info</th>
      {/* <th scope="col">Handle</th> */}
    </tr>
  </thead>
  <tbody>
  {topcourses && topcourses.map((user)=>(

    <tr>
    {/* <th scope="row">1</th> */}
    <td>{user.name}</td>
    <td>{user.enrolledUsersCount}</td>
    <td>
      <button data-bs-toggle="modal" data-bs-target="#staticBackdrop" onClick={() => {
      fetchcourseextra(user._id)}
      }>
      <i class="fa-solid fa-chevron-down" ></i>
      </button>
    </td>
    {/* <td>@mdo</td> */}
  </tr>  
      ))
    }
  </tbody>
</table>
{/* <div className="extraInfo">
    <ul>
        {courseextra && Object.entries(courseextra).map(([key, value]) => (
          <li key={key}>
            <strong>{key}:</strong> {value}
          </li>
        ))}
      </ul>

{courseextra && <CourseDetails courseData={courseextra} /> }
    </div> */}
    </div>
  </div>
  </div>
  </div>
  {/* <!-- Modal --> */}
<div class="modal fade" id="staticBackdrop"  tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      {courseextra && <CourseDetails courseData={courseextra} /> }
      </div>
    </div>
  </div>
</div>

  {/* <!-- Modal --> */}
<div class="modal fade" id="staticBackdrop2"  tabindex="-1" aria-labelledby="staticBackdropLabel2" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      {userextra && <UserProfile user={userextra} /> }
      </div>
    </div>
  </div>
</div>
    </div>
  )
}
