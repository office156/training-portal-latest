import React from 'react';

function UserProfile({ user }) {
  return (
    <div>
      <p>Name: {user.name}</p>
      <p>Email: {user.email}</p>
      <p>Phone: {user.phone}</p>
      <p>Address: {user.address}</p>
      <p>Institute: {user.institute}</p>
      <p>Role: {user.role.join(', ')}</p>
      <p>Education Qualification: {user.eduqua}</p>
      <p>Domain: {user.domain}</p>
    </div>
  );
}

export default UserProfile;
