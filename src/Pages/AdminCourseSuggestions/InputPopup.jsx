import React, { useState } from 'react';
import Modal from 'react-modal';

const InputPopup = ({ isOpen, closeModal, buttonRef }) => {
  const [inputValue, setInputValue] = useState('');

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleSubmit = () => {
    // Do something with the input value (e.g., save it or use it as needed)
    console.log(inputValue);

    // Close the popup
    closeModal();
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={closeModal}
      contentLabel="Input Popup"
      style={{
        content: {
          position: 'absolute',
          top: buttonRef.current.offsetTop + buttonRef.current.clientHeight + 'px',
          left: buttonRef.current.offsetLeft + 'px',
          right: 'auto',
          bottom: 'auto',
        },
      }}
    >
      <div className="popup-content">
        <h2>Input Popup</h2>
        <input
          type="text"
          placeholder="Enter something"
          value={inputValue}
          onChange={handleInputChange}
        />
        <button onClick={handleSubmit}>Submit</button>
        <button onClick={closeModal}>Cancel</button>
      </div>
    </Modal>
  );
}

export default InputPopup;
