import React, { useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
// import SidebarInstructor from '../../../Components/SidebarInstructor/SidebarInstructor';
import { useRef } from 'react';
import { CircularProgress, Button } from '@mui/material';
import Sidebar from '../../Components/Sidebar/Sidebar';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import "./Suggestions.css"


function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;
  


  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};





export default function AdminCourseSuggestions() {


  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = useState([])
  const [suggesstions , setsuggesstions] = useState([]);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [lessonId , setlessonId] = useState("");
  const [lessonTitle , setlessonTitle] = useState("");
  const [inputValue, setInputValue] = useState('');
  const [videourl, setvideourl] = useState('');
  const [pdfurl, setpdfurl] = useState('');

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const AddSuggestion = async() => {
    // Do something with the input value (e.g., save it or use it as needed)
    console.log(inputValue);

    const url2 = "http://localhost:8000/api/admin/suggestion/course/" + courseFromSessionStorage + "/lesson/"+ lessonId
    console.log(lessonTitle)

    const text = lessonTitle + " :- " + inputValue

      try {
        const response = await axios.post(url2, { text });
  
        // Handle the response here, e.g., display a success message
        setInputValue("")
        console.log(response.data);
      } catch (error) {
        // Handle errors, e.g., show an error message
        console.error('Error posting data:', error);
      }

      getAllSuggestions()
      handleClose()

  };

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };


  
  const courseFromSessionStorage = sessionStorage.getItem('updateCourse');
  const token = sessionStorage.getItem('Token');

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    // /admin/courses/:courseId/suggestions
    const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/getlessons"
    
    axios.get(url,{
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }).then((response) => {
      setRows(response.data)
      console.log(response.data)
    })
      .catch((error) => {
        toast.error(error)
      });

  
      getAllSuggestions()
  }, []);


  const getAllSuggestions = ()=>{
    const url2 = "http://localhost:8000/api/admin/courses/"+ courseFromSessionStorage +"/suggestions" 
    axios.get(url2).then((response) => {
      setsuggesstions(response.data)
      console.log(response.data)
    })
      .catch((error) => {

        toast.error(error)
      });
  }

  const getVideo = (lesson) =>{
    // /admin/course/:courseId/lesson/:lessonId/video
    // const url2 = "http://localhost:8000/api/admin/course/"+ courseFromSessionStorage +"/lesson/" +lesson + "/video" 
    // axios.get(url2).then((response) => {
      setvideourl("http://localhost:8000/api/admin/course/"+ courseFromSessionStorage +"/lesson/" +lesson + "/video")
    //   console.log(response.data)
    // })
    //   .catch((error) => {

    //     toast.error(error)
    //   });
  }

  const getPDF = (lesson) =>{
    // /admin/course/:courseId/lesson/:lessonId/video
    // const url2 = "http://localhost:8000/api/admin/course/"+ courseFromSessionStorage +"/lesson/" +lesson + "/video" 
    // axios.get(url2).then((response) => {
      setpdfurl("http://localhost:8000/api/admin/course/"+ courseFromSessionStorage +"/lesson/" +lesson + "/pdf")
    //   console.log(response.data)
    // })
    //   .catch((error) => {

    //     toast.error(error)
    //   });
  }

  // const DeleteLesson = (id) => {
  //   const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/lesson/" + id +"/delete-lesson"
  //   axios.delete(url).then((response)=>{
  //     toast.success("Lesson deleted successfully")
  //   }).catch((error)=>{
  //     console.log(error)
  //   })
  // };

  const DeleteLesson = async (id) => {
    
    
    const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/lesson/" + id +"/delete-lesson"

   await axios.delete(url,{
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }).then((response)=>{
      console.log(response)
      toast.success("Lesson deleted successfully");

      // Reset all lessons
      const url2 = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/getlessons"
      axios.get(url2,{
        headers: {
          'Authorization': `Bearer ${token}`,
        }
      }).then((response) => {
        setRows(response.data)
        console.log(response.data)
      })
        .catch((error) => {
          toast.error(error)
        });



    }).catch((error)=>{
      toast.error(error);
      console.log(error);
    })
  };

 


  return (
    <div className='userListTop'>
            <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2" className='mb-3'>
            Enter your suggestion
          </Typography>
          {/* <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
          </Typography> */}
          <TextField id="outlined-basic" label="suggestion" variant="outlined" sx={{width:"100%"}} value={inputValue}
          onChange={handleInputChange}/>
          <br />
          <div className='mt-5 text-end'>
          <Button variant="outlined" onClick={AddSuggestion}>Enter</Button>
          </div>
          
        </Box>
      </Modal>
      <Sidebar className="adminPanelSidebartag"></Sidebar>
      <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Lessons
          </div>
          <div className="headingUserList">
          Suggestions
          </div>
        </div>
        <div className="tableUsers container">
          <div className="row">
            <div className="col-sm-6">
            <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell align="right">Content</TableCell>
                  <TableCell align="right">Video</TableCell>
                  <TableCell align="right">PDF</TableCell>
                  <TableCell align="right">Suggestions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : rows
                ).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.title}
                    </TableCell>
                    <TableCell align="right">{row.content}</TableCell>
                    {/* <TableCell align="right">{row.category}</TableCell> */}
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={()=>{
                        getVideo(row._id)
                      }}>
                        <i class="fa-solid fa-video deleteIcon border p-2"></i>
                      </button>

                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={()=>{
                        getPDF(row._id)
                      }}>
                        <i class="fa-solid fa-file-pdf deleteIcon border p-2"></i>
                      </button>

                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={()=>{
                        setlessonId(row._id)
                        setlessonTitle(row.title)
                        handleOpen()
                      }}>
                        <i class="fa-solid fa-pen deleteIcon border p-2"></i>
                      </button>

                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
            </div>

            <div className="col-sm-6 adminAllSuggestionsBox">
              <div className='suggList'>
              <ul>
      {suggesstions && suggesstions.map((item, index) => (
        <li key={index}>{item.suggestion.text}</li>
      ))}
    </ul>
              </div>
            </div>

          </div>

        </div>


        <div className='mt-3 container'>
          <h4>Video and PDF</h4>

<div className='d-flex justify-content-around'>
<div>
            <p>PDF:</p>
            {pdfurl && <iframe width="480" height="360" src={pdfurl}></iframe>}


          </div>
          <div>
          
            <p>VIDEO:</p>
            {videourl && <video width="480" height="360" autoPlay controls src={videourl}></video>}
          </div>
</div>
        </div>

      
      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      

    </div>
  )
}

