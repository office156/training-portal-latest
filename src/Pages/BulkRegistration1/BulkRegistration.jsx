import React, { useState, useRef } from 'react'
import DragDropFiles from '../components/DragDropFiles'
import "./BulkRegistration.css"
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';

export default function BulkRegistration() {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setemail] = useState("");
  const [file, setFile] = useState(null);
  const inputRef = useRef(null);

  const handleDrop = (e) => {
    e.preventDefault();
    const newFile = e.dataTransfer.files[0];
    setFile(newFile);
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

  const handleFileInputChange = (event) => {
    const file = event.target.files[0];
    console.log("in change function")
    console.log(file.name)
    setFile(file);
  };

  const handleButtonClick = () => {
    inputRef.current.click();
  };

  const [excel, setexcel] = useState(null);
  const inputRef1 = useRef(null);

  const handleDrop1 = (e) => {
    e.preventDefault();
    const newFile = e.dataTransfer.files[0];
    setexcel(newFile);
  };

  const handleDragOver1 = (e) => {
    e.preventDefault();
  };

  const handleFileInputChange1 = (event) => {
    const file = event.target.files[0];
    console.log("in change function")
    console.log(file.name)
    setexcel(file);
  };

  const handleButtonClick1 = () => {
    inputRef1.current.click();
  };

  function selectOption(option) {
    const element = document.getElementById('dropdownMenuButton');
    if (element) {
      element.innerHTML = option;
    }
  }

  function selectOption1(option) {
    const element = document.getElementById('dropdownMenuButton1');
    if (element) {
      element.innerHTML = option;
    }

  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (name === "" || email === "" || file === null || excel === null) {
      toast.error("Please enter all the details")
    } else {
      const data = { "name": name, "email": email, "aadharcard": file, "excel": excel };

      axios.post('http://localhost:8000/api/register/bulk', data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then((response) => {
        console.log(response)
        setName('');
        setemail('');
        navigate("/bulkregdone")

      })
        .catch((error) => {
          toast.error(error.response.data)

        });
    }
  }

  const handleDownloadExcelFormat = () => {
    axios({
      url: 'http://localhost:8000/api/admin/get-excel-format-document/642d1e84f36607af590ae5ed',
      method: 'GET',
      responseType: 'blob',
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'bulkFormat.xlsx');
      document.body.appendChild(link);
      link.click();
    });
  };

  return (
    <div className='container BulkRegistrationTop'>
      <div className="BulkHeading text-center">
        <p className="bulkHeader">Bulk Registration</p>
        <p className="bulkSubtext">In case of bulk registration, kindly download the excel sheet below, fill the information <br />
          and upload by clicking on the link below.</p>
      </div>
      <div className='centerItems'>
        <div className="fileDownload d-flex justify-content-center" onClick={handleDownloadExcelFormat}>
          <i class="fa-regular fa-file iconAdjust"></i>
          <div className="fileDownloadBulk d-flex justify-content-between align-items-center">
            <p className="fileName">Bulk_registration.xls <br /><span className='fileSize'>200KB</span> </p>
            <i class="fa-solid fa-cloud-arrow-down downloadIconAdjust"></i>
          </div>
        </div>
      </div>

      <form action="">
        <div className="row d-flex justify-content-between">
          <div className="col-12">
            <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Name<span class="text-danger"> *</span></label> <input className='form-control' type="text" id="fname" name='fname' placeholder="Enter your name" onChange={e => setName(e.target.value)} required></input> </div>
            <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Email<span class="text-danger"> *</span></label> <input className='form-control' type="email" placeholder="Enter your email" onChange={e => setemail(e.target.value)} required></input> </div>

            <div className="row py-2">
              <label class="form-control-label form-control-bold py-1">Aadhar card upload (Kindly upload your aadhar card here)<span class="text-danger"> *</span></label>
              <div>
                <div
                  className="d-flex align-items-center justify-content-center"
                  onDrop={handleDrop}
                  onDragOver={handleDragOver}
                  style={{ border: "1px solid #ced4da", height: "150px", borderRadius: "0.375rem" }}
                >
                  <input
                    className="inputWithoutBorder"
                    type="file"
                    ref={inputRef}
                    onChange={handleFileInputChange}
                    hidden
                    accept="image/png, image/jpeg"
                    required
                  />

                  <div className="text-center">
                    <button type="button" className="stop-transition" onClick={handleButtonClick}>
                      {/* <FontAwesomeIcon icon="fa-regular fa-cloud-arrow-up" /> */}
                      <i class="fa-solid fa-cloud-arrow-up"></i>
                    </button>
                    <p className="change-color"><span className="make-bold">Click to upload</span> or drag and drop <br />
                      PNG,JPG (max 5 MB)</p>
                  </div>

                </div>
                {file && (
                  <div>
                    <p>File name: {file.name}</p>
                    <p>File type: {file.type}</p>
                    <p>File size: {file.size} bytes</p>
                  </div>
                )}
              </div>
            </div>

            <div className="row pb-5">
              <label class="form-control-label form-control-bold py-1">Excel file upload (Kindly upload filled excel sheet here)<span class="text-danger"> *</span></label>
              <div>
                <div
                  className="d-flex align-items-center justify-content-center"
                  onDrop={handleDrop1}
                  onDragOver={handleDragOver1}
                  style={{ border: "1px solid #ced4da", height: "150px", borderRadius: "0.375rem" }}
                >
                  <input
                    className="inputWithoutBorder"
                    type="file"
                    ref={inputRef1}
                    onChange={handleFileInputChange1}
                    hidden
                    accept="image/png, image/jpeg"

                  />

                  <div className="text-center">
                    <button type="button" className="stop-transition" onClick={handleButtonClick1}>
                      {/* <FontAwesomeIcon icon="fa-regular fa-cloud-arrow-up" /> */}
                      <i class="fa-solid fa-cloud-arrow-up"></i>
                    </button>
                    <p className="change-color"><span className="make-bold">Click to upload</span> or drag and drop <br />
                      PNG,JPG (max 5 MB)</p>
                  </div>

                </div>
                {excel && (
                  <div>
                    <p>File name: {excel.name}</p>
                    <p>File type: {excel.type}</p>
                    <p>File size: {excel.size} bytes</p>
                  </div>
                )}
              </div>
            </div>

            <div className="registrationSheet text-center">
              <button type='button' className='registrationSheetButton' onClick={handleSubmit}>Send Registration Sheet</button>
            </div>
          </div>
        </div>
      </form>

    </div>
  )
}