import React, { useState , useRef } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "./DragDrop.css"
// import { fa-cloud-arrow-up } from '@fortawesome/free-solid-svg-icons';

function DragDropFiles() {
  const [file, setFile] = useState(null);
  const inputRef = useRef(null);

  const handleDrop = (e) => {
    e.preventDefault();
    const newFile = e.dataTransfer.files[0];
    setFile(newFile);
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

  const handleFileInputChange = (event) => {
    const file = event.target.files[0];
    console.log("in change function")
    console.log(file.name)
    setFile(file);
  };

  const handleButtonClick = () => {
    inputRef.current.click();
  };

  return (
    <div>
      <div
      className="d-flex align-items-center justify-content-center"
        onDrop={handleDrop}
        onDragOver={handleDragOver}
        style={{ border: "1px solid #ced4da", height: "150px", borderRadius: "0.375rem" }}
      >
        <input 
        className="inputWithoutBorder"
            type="file"
            ref={inputRef}
            onChange={handleFileInputChange}
            hidden
            accept="image/png, image/jpeg"
            
          />


          <div className="text-center">
          <button type="button" className="stop-transition" onClick={handleButtonClick}>
          {/* <FontAwesomeIcon icon="fa-regular fa-cloud-arrow-up" /> */}
          <i class="fa-solid fa-cloud-arrow-up"></i>
          </button>
          <p className="change-color"><span className="make-bold">Click to upload</span> or drag and drop <br/>
          PNG,JPG (max 5 MB)</p>
          </div>

      </div>
      {file && (
        <div>
          <p>File name: {file.name}</p>
          <p>File type: {file.type}</p>
          <p>File size: {file.size} bytes</p>
        </div>
      )}
    </div>
  );
}

export default DragDropFiles;
