import React, { useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import SidebarInstructor from '../../../Components/SidebarInstructor/SidebarInstructor';
import "./InstructorCourses.css"
import { useRef } from 'react';
import { CircularProgress, Button } from '@mui/material';
import BasicTable from '../../../Components/SimpleMuiTableCourses/SimpleMuiTableCourses';


function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};





export default function InstructorCourses() {


  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = useState([])
  const [suggestions, setsuggestions] = useState([])
  const [name , setname] = useState("");
  const [description , setdescription] = useState("");
  const [price , setprice] = useState("");
  const [category , setcategory] = useState("");
  const [published , setpublished] = useState("");
  const [image , setimage] = useState(null);
  const [whatulearn , setwhatulearn] = useState("");
  const [prerequisites , setprerequisites] = useState("");
  const [selectedcourseid , setselectedcourseid] = useState();
  const [loading, setLoading] = useState(false);

  const fileInputRef = useRef(null);

  
  const userFromSessionStorage = sessionStorage.getItem('InstructorId');
  const token = sessionStorage.getItem('Token');

  

  const check = "E:/training-portal/src/Images/ales.jpg"

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    const url = "http://localhost:8000/api/course/instructor/" + userFromSessionStorage;
    // console.log(url);
    // console.log(userFromSessionStorage);
    axios.get(url).then((response) => {
      console.log(response.data)
      setRows(response.data.courses)
    })
      .catch((error) => {
        // console.log(error)
        // console.log(error.response.data)
        // alert()
      });
      handleSearch()
  }, []);

  const [filteredItems, setFilteredItems] = useState(rows);
  const [allcourses, setallcourses] = useState(rows);
  
  const handleSearch = () => {
    const filteredResults = rows.filter(item =>
      item.adminApproval === true
    );
    setFilteredItems(filteredResults);
    const filteredResults1 = rows.filter(item =>
      item.adminApproval === false
    );
    setallcourses(filteredResults1);
    console.log(filteredItems)
  };
  // const handleSearch1 = () => {
  //   const filteredResults = rows.filter(item =>
  //     item.adminApproval === true
  //   );
  //   setFilteredItems(filteredResults);
  //   console.log(filteredItems)
  // };


  const handleAction = (id) => {
    const userConfirmed = window.confirm('Are you sure you want to perform this action?');

    if (userConfirmed) {
      DeleteCourse(id)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const PublishCourse = async (id) => {
    // /instructor/request-publish/:courseId", instructorRequestCoursePublish
    const url = "http://localhost:8000/api/instructor/request-publish/" + id 
    await axios.post(url,{
       headers: {
         'Authorization': `Bearer ${token}`,
       }
     }).then((response)=>{
       console.log(response)
       toast.success("Publish requested successfully");
 
       // Reset all courses 
       const url2 = 'http://localhost:8000/api/course/instructor/' + userFromSessionStorage;
      axios.get(url2).then((response) => {
         //   console.log(response.data)
           setRows(response.data.courses)
           console.log(response.data.courses)
         })
           .catch((error) => {
             // console.log(error)
             // console.log(error.response.data)
             // alert()
           });
 
 
 
     }).catch((error)=>{
       toast.error(error);
       console.log(error);
     })
  }

  const handlepublishAction = (id) => {
    const userConfirmed = window.confirm('Are you sure you want to perform this action?');

    if (userConfirmed) {
      PublishCourse(id)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const deleteSuggestion = async (lessonid , suggestionid) =>{
    // /lesson/:lessonId/delete-suggestions/:suggestionId
    // /lesson/:lessonId/delete-suggestions/:suggestionId
    // /course/:courseId/lesson/:lessonId/suggestion/:suggestionId
    const url = "http://localhost:8000/api/delete-suggestion/" + suggestionid
    // const url = "http://localhost:8000/api/course/"+ selectedcourseid +"/lesson/" + lessonid + "/suggestion/" + suggestionid
    console.log(lessonid)
    console.log(suggestionid)
    await axios.delete(url).then((response)=>{
       console.log(response)
       FetchSuggestions(selectedcourseid)
       toast.success("Suggestion deleted successfully");
  
     }).catch((error)=>{
       toast.error(error);
       console.log(error);
     })

  }

  const DeleteCourse = async (id) => {
    
    const url = "http://localhost:8000/api/course/" + id + "/delete-course"
   await axios.delete(url,{
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }).then((response)=>{
      console.log(response)
      toast.success("Course deleted successfully");

      // Reset all courses 
      const url2 = 'http://localhost:8000/api/course/instructor/' + userFromSessionStorage;
     axios.get(url2).then((response) => {
        //   console.log(response.data)
          setRows(response.data.courses)
          console.log(response.data.courses)
        })
          .catch((error) => {
            // console.log(error)
            // console.log(error.response.data)
            // alert()
          });



    }).catch((error)=>{
      toast.error(error);
      console.log(error);
    })
  };

  const UpdateCourse = (id) => {
    sessionStorage.setItem('updateCourse',id);
    navigate("/update-course")
  };

  const AddNewCourse = async(event) => {
      setLoading(true);
    event.preventDefault();
    if (name === "" || description === "" || price === "" || category === "" || published === "" || image === null || whatulearn === "" || prerequisites === "" ) {
      toast.error("Please enter all the details")
    } else {
      const data = { "name" : name, "description" : description , "price":price , "category":category , "published":published , "image":image , "whatyouwilllearn":whatulearn , "prerequisites":prerequisites };
    console.log(data);
    await  axios.post('http://localhost:8000/api/create', data ,  {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      }).then((response) => {
        console.log(response)
        setname('');
        setdescription('');
        setprerequisites('');
        setprice('');
        setcategory('');
        setimage(null);
        fileInputRef.current.value = '';
        setwhatulearn('');
        setpublished('');
        // navigate("/done")
              // Reset all courses 
      const url2 = 'http://localhost:8000/api/course/instructor/' + userFromSessionStorage;
      axios.get(url2).then((response) => {
         //   console.log(response.data)
           setRows(response.data.courses)
           console.log(response.data.courses)
         })
           .catch((error) => {
             console.log(error)
             // console.log(error.response.data)
             // alert()
           });
      })
      .catch((error)=>{
        console.log(error)
        // console.log(error.response.data)
        // alert()
        toast.error(error.response.data.message)

      });
    }
      setLoading(false);

  };

  const FetchSuggestions = (id) =>{
    const url2 = "http://localhost:8000/api/course-all-suggestions/"+ id ;
    // const url2 = "http://localhost:8000/api/courses/"+ id + "/suggestions";
    axios.get(url2).then((response) => {
       //   console.log(response.data)
         setsuggestions(response.data)
         console.log(response.data)
         console.log(response)
       })
         .catch((error) => {
           // console.log(error)
           // console.log(error.response.data)
           // alert()
         });

  }

  const PatchCourse = async(event) => {
    event.preventDefault();
    if (name === "" || description === "" || price === "" || category === "" || published === "" || image === null || whatulearn === "" || prerequisites === "" ) {
      toast.error("Please enter all the details")
    } else {
      const data = { "name" : name, "description" : description , "price":price , "category":category , "published":published , "image":image , "whatyouwilllearn":whatulearn , "prerequisites":prerequisites };
    console.log(data);
    await  axios.post('http://localhost:8000/api/create', data ,  {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      }).then((response) => {
        console.log(response)
        setname('');
        setdescription('');
        setprerequisites('');
        setprice('');
        setcategory('');
        setimage(null);
        fileInputRef.current.value = '';
        setwhatulearn('');
        setpublished('');
        // navigate("/done")
      })
      .catch((error)=>{
        console.log(error)
        // console.log(error.response.data)
        // alert()
        toast.error(error.response.data.message)

      });


      // Reset all courses 
      const url2 = 'http://localhost:8000/api/course/instructor/' + userFromSessionStorage;
     await axios.get(url2).then((response) => {
        //   console.log(response.data)
          setRows(response.data.courses)
          console.log(response.data.courses)
        })
          .catch((error) => {
            // console.log(error)
            // console.log(error.response.data)
            // alert()
          });
    }
  };

  const navigateToTheCourse = (courseId) => {
    // console.log('Course ID : ' + courseId + " "  + courseName);
    sessionStorage.setItem('selectedCourseId', courseId)
    navigate('/course-info')
  } 

  return (
    <div className='userListTop'>
      <SidebarInstructor className="adminPanelSidebartag"></SidebarInstructor>
      <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Courses
          </div>
          {/* <div className="UserListLinks d-flex justify-content-between">
            <div className="UserListTopLink1 px-3">Download in excel file</div>
            <div className="UserListTopLink1">Create New User</div>
          </div> */}
        </div>
        <div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Price</TableCell>
                  <TableCell align="right">Category</TableCell>
                  <TableCell align="right">Suggestions</TableCell>
                  <TableCell align="right">Update</TableCell>
                  <TableCell align="right">Delete</TableCell>
                  <TableCell align="right">Publish</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? allcourses.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : allcourses
                ).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      <span onClick={() => navigateToTheCourse(row._id)}>{row.name}</span>
                    </TableCell>
                    <TableCell align="right">{row.price}</TableCell>
                    <TableCell align="right">{row.category}</TableCell>
                    <TableCell align="right">
                    <button type='button' className='ApproveUser' data-bs-toggle="modal" data-bs-target="#suggestionsModal">
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        {/* <i class="fa-solid fa-pen deleteIcon border p-2" onClick={() => UpdateCourse(row._id) }></i> */}
                        <i class="fa-solid fa-message deleteIcon border p-2" onClick={() => {
                          // sessionStorage.setItem("selectedCourseId",row._id)
                          setselectedcourseid(row._id)
                          FetchSuggestions(row._id)
                        } }></i>
                      </button>
                    </TableCell>
                    <TableCell align="right">
                    <button type='button' className='ApproveUser' >
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-pen deleteIcon border p-2" onClick={() => UpdateCourse(row._id) }></i>
                      </button>
                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' >
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-trash deleteIcon border p-2" onClick={() => handleAction(row._id) }></i>
                      </button>
                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' >
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-upload deleteIcon border p-2" onClick={() => handlepublishAction(row._id) }></i>
                      </button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>

        <div className="AddCourseButton text-center mt-5">
        {/* <button className='ApproveUser' onClick={AddNewCourse}>
        <i class="fa-solid fa-plus mr-2 deleteIcon border p-2"></i>
        <br />
            <span className="buttonText">Add new course</span>
        </button> */}
        <button type="button" class="p-2 btn-register " data-bs-toggle="modal" data-bs-target="#exampleModal">
                    {/* <i class="fa-solid fa-plus" ></i>  */}
                    {/* <br /> */}
                    <span className="">Add new course</span>
</button>
      </div>

              {/* <!-- Modal --> */}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">New Course</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Name:</label>
            <input type="text" class="form-control" value={name} onChange={e => setname(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Description:</label>
            <input type="text" class="form-control" value={description} onChange={e => setdescription(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Price:</label>
            <input type="number" class="form-control" value={price} onChange={e => setprice(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Category:</label>
            <input type="text" class="form-control" value={category} onChange={e => setcategory(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">What you will learn:</label>
            <input type="text" class="form-control" value={whatulearn} onChange={e => setwhatulearn(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Published:</label>
            {/* <input type="text" class="form-control" value={published} onChange={e => setpublished(e.target.value)}></input> */}

{/* Radio buttons  */}
<div>
  <label for="f-option" class="l-radio">
    <input type="radio" id="f-option" value="true" name="selector" tabindex="1" onChange={(e)=>{
      setpublished(e.target.value)
    }}></input>
    <span>Yes</span>
  </label>
  <label for="s-option" class="l-radio">
    <input type="radio" id="s-option" value="false" name="selector" tabindex="2" onChange={(e)=>{
      setpublished(e.target.value)
    }}></input>
    <span>No</span>
  </label>
</div>

          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Prerequisites:</label>
            <input type="text" class="form-control" value={prerequisites} onChange={e => setprerequisites(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Image:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setimage(e.target.files[0])} ref={fileInputRef}/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="" onClick={AddNewCourse}>Add</button>
        {/* <button type="button" class="" onClick={()=>SendEmail(selectedEmail)}>Send email</button> */}
        
      </div>
    </div>
  </div>
</div>

<div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Live courses
          </div>
          {/* <div className="UserListLinks d-flex justify-content-between">
            <div className="UserListTopLink1 px-3">Download in excel file</div>
            <div className="UserListTopLink1">Create New User</div>
          </div> */}
        </div>

        <div className='container'>
          <BasicTable rows={filteredItems} ></BasicTable>
        </div>





{/* <!-- Modal --> */}
<div class="modal fade" id="suggestionsModal" tabindex="-1" aria-labelledby="suggestionsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content ">
      <div class="modal-body ">
      <ul>
      {suggestions && suggestions.map((item, index) => (
        <li key={index}>{item.suggestion.text}
        <button onClick={() => deleteSuggestion(item.lessonId , item.suggestion._id) }>delete</button>
        </li>
      ))}
    </ul>



{/* tailwind content  */}

{/* <div class="w-full h-full overflow-auto shadow bg-white" id="journal-scroll">

<table class="w-full">


    <tbody class="">
                                <tr class="relative transform scale-100
                    text-xs py-1 border-b-2 border-blue-100 cursor-default

            bg-blue-500 bg-opacity-25">
            <td class="pl-5 pr-3 whitespace-no-wrap">
                <div class="text-gray-400">Today</div>
                <div>07:45</div>
            </td>

            <td class="px-2 py-2 whitespace-no-wrap">
                <div class="leading-5 text-gray-500 font-medium">Taylor Otwel</div>
                <div class="leading-5 text-gray-900">Create pull request #1213
                    <a class="text-blue-500 hover:underline" href="#">#231231</a></div>
                <div class="leading-5 text-gray-800">Hello message</div>
            </td>

        </tr>
      
      </tbody>
</table>

</div> */}
{/* end  */}



      </div>
    </div>
  </div>
</div>
      
      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      

    </div>
  )
}

