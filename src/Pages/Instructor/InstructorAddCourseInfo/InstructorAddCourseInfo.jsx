import React, { useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import SidebarInstructor from '../../../Components/SidebarInstructor/SidebarInstructor';
import { useRef } from 'react';
import { CircularProgress, Button } from '@mui/material';


function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;
  


  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};





export default function InstructorAddCourseInfo() {


  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = useState([])
  const [video , setVideo] = useState(null);
  const [pdf , setpdf] = useState(null);
  const [title , settitle] = useState("");
  const [content , setContent] = useState("");
  const [course,setCourse] = useState();
  const [updateLesson,setupdateLesson] = useState();
  // const [name , setname] = useState("");
  // const [description , setdescription] = useState("");
  // const [price , setprice] = useState("");
  // const [category , setcategory] = useState("");
  // const [published , setpublished] = useState("");
  // const [image , setimage] = useState(null);
  // const [whatulearn , setwhatulearn] = useState("");
  // const [prerequisites , setprerequisites] = useState("");
  const [loading, setLoading] = useState(false);
  
  const fileInputRef = useRef(null);
  const fileInputRef2 = useRef(null);

  
  const courseFromSessionStorage = sessionStorage.getItem('updateCourse');
  const token = sessionStorage.getItem('Token');

  

  // const check = "E:/training-portal/src/Images/ales.jpg"

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/getlessons"
    const url2 = "http://localhost:8000/api/coursedetails/" + courseFromSessionStorage
    axios.get(url,{
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }).then((response) => {
      setRows(response.data)
      console.log(response.data)
    })
      .catch((error) => {
        toast.error(error)
      });

    // axios.get(url2).then((response) => {
    //   setCourse(response.data)
    // })
    //   .catch((error) => {

    //     toast.error(error)
    //   });
  }, []);


  // const DeleteLesson = (id) => {
  //   const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/lesson/" + id +"/delete-lesson"
  //   axios.delete(url).then((response)=>{
  //     toast.success("Lesson deleted successfully")
  //   }).catch((error)=>{
  //     console.log(error)
  //   })
  // };

  const DeleteLesson = async (id) => {
    
    
    const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/lesson/" + id +"/delete-lesson"

   await axios.delete(url,{
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }).then((response)=>{
      console.log(response)
      toast.success("Lesson deleted successfully");

      // Reset all lessons
      const url2 = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/getlessons"
      axios.get(url2,{
        headers: {
          'Authorization': `Bearer ${token}`,
        }
      }).then((response) => {
        setRows(response.data)
        console.log(response.data)
      })
        .catch((error) => {
          toast.error(error)
        });



    }).catch((error)=>{
      toast.error(error);
      console.log(error);
    })
  };

  const UpdateLesson = async (id) => {
    // event.preventDefault();
    // if (title === "" || content === "" || pdf === null || video === null ) {
    //   toast.error("Please enter all the details")
    // } else {
      // localhost:8000/api/course/64411451f03aa58a5c80f7b7/lesson/6458bd2846e04ad139df4708/patch
      // const url = 
      const dataTest = { "title" : title, "content" : content , "pdf":pdf , "video":video };
    console.log(dataTest);
    const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/lesson/" + id +"/patch"
    await  axios.patch(url, dataTest ,  {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      }).then((response) => {
        console.log(response)
        setContent('');
        settitle('');
        setVideo(null);
        setpdf(null);
        fileInputRef.current.value = '';
        fileInputRef2.current.value = '';

              // Reset all lessons 
      const url2 = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/getlessons"
      axios.get(url2,{
        headers: {
          'Authorization': `Bearer ${token}`,
        }
      }).then((response) => {
        setRows(response.data)
      })
        .catch((error) => {
          console.log(error)
          toast.error(error)
        });

      })
      .catch((error)=>{
        toast.error(error.response.data.message)
        console.log(error);
      });



    
  };

  const AddNewLesson = async (event) => {
    event.preventDefault();
    setLoading(true);

    if (title === "" || content === "" || pdf === null || video === null ) {
      toast.error("Please enter all the details")
    } else {
      const dataTest = { "title" : title, "content" : content , "pdf":pdf , "video":video };
    console.log(dataTest);
    const url = "http://localhost:8000/api/course/" + courseFromSessionStorage +"/addlesson"
   await axios.post(url, dataTest ,  {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': `Bearer ${token}`
        }
      }).then((response) => {
        console.log('Adding a new lesson ')
        console.log(response.data)
        setContent('');
        settitle('');
        setVideo(null);
        setpdf(null);
        fileInputRef.current.value = "";
        fileInputRef2.current.value = "";

        console.log("After removing everything")

              // Reset all lessons 
      const url2 = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/getlessons"
      axios.get(url2,{
        headers: {
          'Authorization': `Bearer ${token}`,
        }
      }).then((response) => {
        console.log("resetting lessons")
        setRows(response.data)
      })
        .catch((error) => {
          toast.error(error)
        });

      })
      .catch((error)=>{
        toast.error(error.response.data.message)
        console.log('Somre error occured in the AddNewLesson '+ error);
      });
    }
    setLoading(false);

  };

  const handlepublishAction = () => {
    const userConfirmed = window.confirm('Are you sure you want to perform this action?');

    if (userConfirmed) {
      PublishCourse()
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const PublishCourse = async () => {
    // /instructor/request-publish/:courseId", instructorRequestCoursePublish
    const url = "http://localhost:8000/api/instructor/request-publish/" + courseFromSessionStorage
    await axios.post(url,{
       headers: {
         'Authorization': `Bearer ${token}`,
       }
     }).then((response)=>{
       console.log(response)
       toast.success("Publish requested successfully");
 
     }).catch((error)=>{
       toast.error(error.response.data.message);
       console.log(error.response.data.message);
     })
  }

  return (
    <div className='userListTop'>
      <SidebarInstructor className="adminPanelSidebartag"></SidebarInstructor>
      <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Lessons
          </div>
          {/* <div className="UserListLinks d-flex justify-content-between">
            <div className="UserListTopLink1 px-3">
              {course["_id"]}
            </div>
            <div className="UserListTopLink1">Create New User</div>
          </div> */}
        </div>
        <div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell align="right">Content</TableCell>
                  {/* <TableCell align="right">Category</TableCell> */}
                  <TableCell align="right">Update</TableCell>
                  <TableCell align="right">Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : rows
                ).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.title}
                    </TableCell>
                    <TableCell align="right">{row.content}</TableCell>
                    {/* <TableCell align="right">{row.category}</TableCell> */}
                    <TableCell align="right">
                    <button type="button" class="ApproveUser" data-bs-toggle="modal" data-bs-target="#UpdateLessonModal">
                    {/* <i class="fa-solid fa-plus" ></i>  */}
                    {/* <br /> */}
                    <span className="" onClick={()=>{
                      setupdateLesson(row._id);
                    }}><i class="fa-solid fa-pen deleteIcon border p-2"></i></span>
</button>
                    {/* <button type='button' className='ApproveUser' >
                        <i class="fa-solid fa-pen deleteIcon border p-2"></i>
                      </button> */}
                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={()=>{
                        DeleteLesson(row._id)
                      }} >
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-trash deleteIcon border p-2"></i>
                      </button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>

        <div className="AddCourseButton d-flex justify-content-between container mt-5">
        {/* <button className='ApproveUser' onClick={AddNewCourse}>
        <i class="fa-solid fa-plus mr-2 deleteIcon border p-2"></i>
        <br />
            <span className="buttonText">Add new course</span>
        </button> */}
        <button type="button" class="p-2 btn-register " data-bs-toggle="modal" data-bs-target="#exampleModal">
                    {/* <i class="fa-solid fa-plus" ></i>  */}
                    {/* <br /> */}
                    <span className="">Add new lesson</span>
</button>

        <button type="button" class="p-2 btn-register " onClick={()=>{
          sessionStorage.setItem('selectedCourseId', courseFromSessionStorage);
          navigate("/instructor-preview")
        }}>
                    <span className="">Preview</span>
</button>

        <button type="button" class="p-2 btn-register " onClick={handlepublishAction}>
                    <span className="">Publish</span>
</button>
      </div>

              {/* <!-- Modal --> */}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">New Lesson</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

        <div>
          <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Title:</label>
            <input type="text" class="form-control" value={title} onChange={e => settitle(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Content:</label>
            <input type="text" class="form-control" value={content} onChange={e => setContent(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Video:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setVideo(e.target.files[0])} ref={fileInputRef}/>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Pdf:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setpdf(e.target.files[0])} ref={fileInputRef2}/>
          </div>
          { loading ? 
      (<CircularProgress />) : 
      (<></>) }
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="" onClick={AddNewLesson}>Add</button>
        {/* <button type="button" class="" onClick={()=>SendEmail(selectedEmail)}>Send email</button> */}
      </div>
        </div>
      
      
    </div>
  </div>
</div>


              {/* <!--Update Lesson Modal --> */}
<div class="modal fade" id="UpdateLessonModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Update Lesson</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Title:</label>
            <input type="text" class="form-control" value={title} onChange={e => settitle(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Content:</label>
            <input type="text" class="form-control" value={content} onChange={e => setContent(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Video:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setVideo(e.target.files[0])} ref={fileInputRef}/>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Pdf:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setpdf(e.target.files[0])} ref={fileInputRef2}/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="" onClick={()=>{
          console.log("You clicked update")
          UpdateLesson(updateLesson);
        }}>Update</button>
        {/* <button type="button" class="" onClick={()=>SendEmail(selectedEmail)}>Send email</button> */}
      </div>
    </div>
  </div>
</div>
      
      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      

    </div>
  )
}

