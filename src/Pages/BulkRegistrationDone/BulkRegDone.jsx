import React from 'react'
import "./BulkRegDone.css"

export default function BulkRegDone
    () {
    return (
        <div className='bulkRegDoneTop text-center container d-flex justify-content-center align-items-center'>
            <p className='p-5 border paraDesign'>  Dear user , <br /> Thank you for sending bulk registration excel sheet. We will share portal usernames and passwords after successful verification.
            </p>
        </div>

    )
}
