import React from 'react'

export default function ImageModal({ imageUrl }) {
  return (
    <div>
       
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Open
</button>


<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        {imageUrl}
        <img src={require(imageUrl)} alt="" />
      </div>
    </div>
  </div>
</div>
    </div>
  )
}
