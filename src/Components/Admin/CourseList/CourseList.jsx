import React, { useState } from 'react';
import "./CourseList.css";
import Sidebar from '../../Sidebar/Sidebar';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};





export default function CourseList() {


  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = useState([])
  const [selectedEmail, setSelectedEmail] = useState();
  const [subject , setSubject] = useState("");
  const [message , setMessage] = useState("");
  const [adminname , setadminname] = useState("")
  const [adminmail , setadminmail] = useState("")

  

  const check = "E:/training-portal/src/Images/ales.jpg"

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    fetchCourses()
    setadminmail(sessionStorage.getItem("email"))
    setadminname(sessionStorage.getItem("name"))
  }, []);

  const fetchCourses = () => {
    axios.get('http://localhost:8000/api/allcourses').then((response) => {
      console.log(response.data)
      setRows(response.data)
    })
      .catch((error) => {
        console.log(error)
      });
  }

  const handleAction = (id) => {
    const userConfirmed = window.confirm('Are you sure you want to perform this action?');

    if (userConfirmed) {
      DeleteCourse(id)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  const UpdateCourse = (id) => {
    sessionStorage.setItem('updateCourse',id);
    navigate("/admin-course-suggestions")
  };

  const DeleteCourse = (id) => {
    const url = "http://localhost:8000/api/admin/course/" + id + "/delete-course"
    axios.delete(url).then((response) => {
      console.log(response.data)
      // setRows(response.data)
      fetchCourses()
    })
      .catch((error) => {
        console.log(error)

      });
  };

  return (
    <div className='userListTop'>
      <Sidebar className="adminPanelSidebartag" name={adminname} mail={adminmail}></Sidebar>
      <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Courses
          </div>
          {/* <div className="UserListLinks d-flex justify-content-between">
            <div className="UserListTopLink1 px-3">Download in excel file</div>
            <div className="UserListTopLink1">Create New User</div>
          </div> */}
        </div>
        <div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Price</TableCell>
                  <TableCell align="right">Category</TableCell>
                  <TableCell align="right">Suggestions</TableCell>
                  <TableCell align="right">Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : rows
                ).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.price}</TableCell>
                    <TableCell align="right">{row.category}</TableCell>
                    <TableCell align="right">
                    <button type='button' className='ApproveUser' onClick={() => UpdateCourse(row._id) }>
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-message deleteIcon border p-2"></i>
                      </button>
                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={() => handleAction(row._id) } >
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-trash deleteIcon border p-2"></i>
                      </button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>

      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      
    </div>
  )
}

