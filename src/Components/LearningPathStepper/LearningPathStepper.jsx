import React from 'react'
import "./Stepper.css"
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import StepContent from '@mui/material/StepContent';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';




const steps = [
    {
      label: 'Foundation (2 Courses)',
      description: `For each ad campaign that you create, you can control how much
                you're willing to spend on clicks and conversions, which networks
                and geographical locations you want your ads to show on, and more.`,
    },
    {
      label: 'Intermediatory (2 Courses)',
      description:
        'An ad group contains one or more ads which target a shared set of keywords.',
    },
    {
      label: 'Advanced (2 Courses)',
      description: `Try out different ad text to see what brings in the most customers,
                and learn how to enhance your ads using features like ad extensions.
                If you run into any problems with your ads, find out how to tell if
                they're running and how to resolve approval issues.`,
    },
  ];

export default function LearningPathStepper() {


    const [activeStep, setActiveStep] = React.useState(0);

    const handleNext = () => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
  
    const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
  
    const handleReset = () => {
      setActiveStep(0);
    };
 
 
    return (
    <div>
        <Box>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => (
          <Step key={step.label} >
            <StepLabel
              optional={
                index === 0 ? (
                  <Typography className='subtextMainLabels' variant="caption">These foundation courses cover introductory part about HPC domain</Typography>
                ) : index === 1 ? (
                    <Typography className='subtextMainLabels' variant="caption">These intermediatory courses covers about OpenMP and MPI</Typography>
                  ) : index === 2 ? (
                    <Typography className='subtextMainLabels' variant="caption">These courses cover about advanced part of HPC domain</Typography>
                  ) : null

              }
            >
                <div className="StepperMainLabels">
                {step.label}
                </div>
              
            </StepLabel>
            <StepContent>
              {/* <Typography>{step.description}</Typography> */}

              <div className="container">
                  <div className="row">
                  <div class="col-md-6 col-lg-4 col-xl-3 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Machine Learning</h5>
                    </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Machine Learning</h5>
                    </div>
            </div>
        </div>
                      <div className="col"></div>
                      <div className="col"></div>
                  </div>
              </div>


              <Box sx={{ mb: 2 }}>
                <div>
                  <Button
                    variant="contained"
                    onClick={handleNext}
                    sx={{ mt: 1, mr: 1 }}
                  >
                    {index === steps.length - 1 ? 'Finish' : 'Continue'}
                  </Button>
                  <Button
                    disabled={index === 0}
                    onClick={handleBack}
                    sx={{ mt: 1, mr: 1 }}
                  >
                    Back
                  </Button>
                </div>
              </Box>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} sx={{ p: 3 }}>
          <Typography>All steps completed - you&apos;re finished</Typography>
          <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
            Reset
          </Button>
        </Paper>
      )}
    </Box>
    </div>
  )
}
