import React from 'react'
import "./Faq.css"
import { useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';

export default function Faq() {

  const [selectedEmail, setSelectedEmail] = useState("");
  const [subject , setSubject] = useState("");
  const [message , setMessage] = useState("");


  function SendEmail () {
    // event.prevent
    if (subject === "" || message === "" || selectedEmail === "") {
      toast.error("Please enter all the details")
    } else {
      const data = { "name": subject, "email": message , "emailId" : selectedEmail };
      
      // const url = "http://localhost:8000/api/admin/user/send/" + email1
      // axios.post(url, data).then((response) => {
      //   console.log(response)
      //   setMessage('');
      //   setSubject('');
      //   toast.success(response.data)
      // })
      //   .catch((error) => {
      //     toast.error(error.response.data)

      //   });

      console.log(data);
      setMessage("");
      setSelectedEmail("");
      setSubject("");
    }
  }

  return (
    <div className="container FaqPageTop">
        <h2 className='FAQheading mb-20'>Frequently Asked Questions</h2>
        <div className="container questionTop">
            <p className="faqQuestions pb-3">What are the benefits for enrolling in the courses?</p>
            <p className="faqQuestions pb-3">Who is eligible for enrolling in the courses?</p>
            <p className="faqQuestions pb-3">Can I enroll to more than one courses at a time?</p>
            <p className="faqQuestions pb-3">What all mode of payments are available on the platform?</p>
            <p className="faqQuestions pb-3">Whether course completion certificate will be issued?</p>

        </div>

        <div className="contactFaq d-flex justify-content-between align-items-center">
            <div className=''>
            <p className="contactFaqTitle">Still have any questions?</p>
            <p className='contactSubtext'>Can’t find the answer you’re looking for? Please contact us by click the button </p>
            </div>
            {/* <button className='getInTouch' type='button'>Get in touch</button> */}
            <button type="button" class="getInTouch" data-bs-toggle="modal" data-bs-target="#exampleModal">Get in touch</button>
        </div>

        {/* <!-- Modal --> */}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">New message</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Email id:</label>
            <input type="email" class="form-control" id="recipient-name" value={selectedEmail} onChange={e => setSelectedEmail(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" id="email-subject" value={subject} onChange={e => setSubject(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="email-text" value={message} onChange={e => setMessage(e.target.value)}></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="" onClick={SendEmail}>Send email</button>
        
      </div>
    </div>
  </div>
</div>
<ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
