import React, { useState } from 'react'
import "./AboutCourse.css"
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import { loadStripe } from '@stripe/react-stripe-js'
import { useLocation } from 'react-router-dom'
import { useEffect } from 'react'


export default function AboutCourse() {

  const userFromSessionStorage = sessionStorage.getItem('userId');
  const courseFromSessionStorage = sessionStorage.getItem('selectedCourseId');
  const token = sessionStorage.getItem('Token');
  // const [selectedCourse , setSelectedCourse] = useState();
  const [courseName, setCourseName] = useState();
  const [courseDescription, setCourseDescription] = useState();
  const [coursePrice, setCoursePrice] = useState();
  const [coursewhatLearn, setCoursewhatLearn] = useState([]);
  const [coursePrerequisites, setCoursePrerequisites] = useState([]);
  const [courseCategory, setCourseCategory] = useState();

  useEffect(() => {
    // const userFromSessionStorage = sessionStorage.getItem('userId');
    // console.log(courseFromSessionStorage)
    const url = "http://localhost:8000/api/coursedetails/" + courseFromSessionStorage
    axios.get(url).then((response) => {
        console.log(response.data)
      //   setSelectedCourse(response.data)
      setCourseName(response.data.name);
      setCoursePrice(response.data.price);
      setCourseCategory(response.data.category);
      setCoursePrerequisites(response.data.prerequisites);
      setCourseCategory(response.data.category);
      setCourseDescription(response.data.description);
      setCoursewhatLearn(response.data.whatyouwilllearn);
    })
      .catch((error) => {
        console.log(error.data)
      });
  }, []);

  return (
    <div className='container'>
      <p className=''>
      <span className='AboutThisCourse'>Course title : </span>
        <span className='InfoAboutCourse'>{courseName}</span>
      </p>
      <div className="InfoCoursePart1">
        <p className="AboutThisCourse">About this course</p>
        <p className="InfoAboutCourse">
          {courseDescription}
        </p>
     
      </div>
      <div className="InfoCoursePart2">
        <p className="WhatYouLearn">What you’ll learn in this course</p>
        <div className="prerequitesText">
          {/* {coursePrerequisites} */}
          <ul>
      {coursewhatLearn.map(item => (
        <li key={item}>{item}</li>
      ))}
    </ul>
        </div>
      </div>
      <div className="CoursePrerequisites">
        <div className="PrerequisitesHeader">
          Prerequisites
        </div>

        <div className="prerequitesText">
          {/* {coursePrerequisites} */}
          <ul>
      {coursePrerequisites.map(item => (
        <li key={item}>{item}</li>
      ))}
    </ul>
        </div>
      </div>
    </div>
  )
}
