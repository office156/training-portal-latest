import React, { useEffect, useState } from 'react';
import axios from 'axios';
import "./CourseLearning.css";

export default function CourseLearning() {
  const [lessons, setLessons] = useState([]);
  const courseIdFromSessionStorage = sessionStorage.getItem('selectedCourseId');
  const tokenFromSessionStorage = sessionStorage.getItem('Token');

  useEffect(() => {
    fetchLessons();
  }, []);

  const fetchLessons = async () => {
    try {
      const url = `http://localhost:8000/api/course/${courseIdFromSessionStorage}/getlessons`;
      const response = await axios.get(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      });
      setLessons(response.data);
    } catch (error) {
      console.error('Error fetching lessons:', error);
    }
  };

  const renderLessons = () => {
    return lessons.map((lesson, index) => (
      <div key={index} className="LessonCard">
        <span className="lessonNo">Lesson {index + 1}:</span>
        <span className="lessonTitle">{lesson.title}</span>
        <i className="fa-solid fa-cloud-arrow-down" onClick={() => handleDownloadPDF(lesson)}></i>
      </div>
    ));
  };

  const handleDownloadPDF = (lesson) => {
    const downloadUrl = `http://localhost:8000/api/course/${courseIdFromSessionStorage}/lesson/${lesson._id}/pdf`;

    axios({
      url: downloadUrl,
      method: 'GET',
      responseType: 'blob',
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute('download', 'file.pdf');
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }).catch((err) => {
      console.error('Error downloading the file:', err);
    });
  };

  return (
    <div className="CourseLearningContainer">
      {renderLessons()}
    </div>
  );
}
