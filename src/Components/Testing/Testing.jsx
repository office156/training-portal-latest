import React, { useState } from 'react';
import axios from 'axios';
import "./Testing.css"
import SearchBar from './SearchBar';
import SearchResults from './SearchResults';

 

export default function Testing() {


    const [aadharcard, setAadharcard] = useState(null);
    const [excel, setExcel] = useState(null);
    const [fileUrl, setFileUrl] = useState('');
    const [imageUrl, setImageUrl] = useState(null);

    async function stripeSuccess() {
        

      try {
          const token ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NDNmZGI1NjFlMzA0Mjk1ODliYzFlNDAiLCJpYXQiOjE2ODE5MDkzNTgsImV4cCI6MTY4MjUxNDE1OH0.dxZffsCVIK2jicqO0osZGSmb8DCqSWZlXfxKisr_spM"
          const {data} = await axios.post("http://localhost:8000/api/course/enroll/success/643fe5cae721e3ebb7e30869",null,{
              headers: {
                'Authorization': `Bearer ${token}`
              }
            })


            console.log(data)
            // window.location.href = data
  
          // const stripe = await loadStripe(process.env.NEXT_PUBLIC_STRIPE_KEY);
          // // stripe.red
          // stripe.redirectToCkeckout({ sessionId : data});
      } catch (error) {
          console.log(error)
      }

  }

    const handleImageLoad = async () => {
      const response = await fetch('http://localhost:8000/api/admin/user/photo/643535d41510924f99f29630');
      const blob = await response.blob();
      const objectUrl = URL.createObjectURL(blob);
      setImageUrl(objectUrl);
    };

    const handleDownload = async () => {
      axios({
        url: 'http://localhost:8000/api/dummycertificate',
        method: 'POST',
        responseType: 'blob',
      }).then((response) => {
        console.log(response)
        const pdfurl = window.URL.createObjectURL(new Blob([response.data],{ type: 'application/pdf' }));
        window.open(pdfurl, '_blank');
        
        // const link = document.createElement('a');
        // link.href = url;
        // link.setAttribute('download', 'certificate.pdf');
        // document.body.appendChild(link);
        // link.click();
      });
    };
  
    const handleDownloadAadharcard = (id) => {
      axios({
        url: 'http://localhost:8000/api/dummycertificate',
        method: 'POST',
        responseType: 'blob',
      }).then((response) => {
        console.log(response)
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'certificate.pdf');
        document.body.appendChild(link);
        link.click();
      });
    };
  
    const handleDownloadExcel = () => {
      axios({
        url: 'api/download-excel',
        method: 'GET',
        responseType: 'blob',
      }).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'bulk.xlsx');
        document.body.appendChild(link);
        link.click();
      });
    };

    const [items, setItems] = useState([
      'Apple',
      'Banana',
      'Cherry',
      'Date',
      'Elderberry',
      // Add more items here
    ]);
  
    const [filteredItems, setFilteredItems] = useState(items);
  
    const handleSearch = (query) => {
      const filteredResults = items.filter((item) =>
        item.toLowerCase().includes(query.toLowerCase())
      );
      setFilteredItems(filteredResults);
    };

  return (
    <div>
            <div>Testing</div>
    <div>
      <button onClick={handleDownloadAadharcard}>Download Aadharcard</button>
      {/* <button onClick={() => ApproveUser(row._id)}>Download Excel</button> */}
    </div>


    <button onClick={handleDownload}>Download File</button>
      {fileUrl && <a href={fileUrl} download>Click here to download the file</a>}

      <div>
      <button onClick={handleImageLoad}>Load image</button>
      {imageUrl && <img src={imageUrl} alt="Loaded image" />}
      <button onClick={stripeSuccess}>stripe</button>
    </div>

    {/* <button id='sendEmailButton'><span>Button</span><i></i></button> */}


    <a href="#" class="button">
    <div class="button__line"></div>
    <div class="button__line"></div>
    <span class="button__text">ENTRY</span>
    <div class="button__drow1"></div>
    <div class="button__drow2"></div>
  </a>

  <button>certificate</button>

  <div>
      <h1>Live Search Example</h1>
      <SearchBar onSearch={handleSearch} />
      <SearchResults results={filteredItems} />
    </div>

    </div>
  )
}
