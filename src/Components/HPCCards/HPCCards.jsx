import React from 'react';
import "./HPCCards.css"

export default function HPCCards() {
  return <div className='LearningPathcardsTop'>
 <div className="container">
        <h3 className='cardsHeader LearningPathMainHeader'>High Performance Computing Courses</h3>
        <div className="cards row">

        <div class="col-md-6 col-lg-4 col-xl-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">HPC for beginners</h5>
                        <div className="hpcCardsSubtext text-start">
                            CDAC
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">OpenMP for beginners</h5>
                                                <div className="hpcCardsSubtext text-start">
                            CDAC
                        </div>
                    </div>
            </div>
        </div>
        
        <div class="col-md-6 col-lg-4 col-xl-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Job Schedulers</h5>
                                                <div className="hpcCardsSubtext text-start">
                            CDAC
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">MPI for beginners</h5>
                                                <div className="hpcCardsSubtext text-start">
                            CDAC
                        </div>
                    </div>
            </div>
        </div>
        
        <div class="col-md-6 col-lg-4 col-xl-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">CUDA programming</h5>
                                                <div className="hpcCardsSubtext text-start">
                            CDAC
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-4 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">SPACK utility</h5>
                                                <div className="hpcCardsSubtext text-start">
                            CDAC
                        </div>
                    </div>
            </div>
        </div>

        </div>
    </div>
  </div>;
}
